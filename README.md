This is an example implementation of the Apriori-algorithm. (http://en.wikipedia.org/wiki/Apriori_algorithm)
It is oriented on the second example in the wikipedia article.

Run with `julia apriori.jl`.

Example output with debugging output activated:

    ------------------ looking at all combinations of size 1 (combs_of_size[n]) -----------------
    those are Set{Array{Int64,1}}({[2],[1],[3],[4]})

    [2] is in 37.5% of the transactions, thus adding it to result.
    [1] is in 37.5% of the transactions, thus adding it to result.
    [3] is in 37.5% of the transactions, thus adding it to result.
    [4] is in 37.5% of the transactions, thus adding it to result.

    finished iteration for combinations of size 1, current result is:
    {[2],[1],[3],[4]}
    ------------------ looking at all combinations of size 2 (combs_of_size[n]) -----------------
    those are Set{Array{Int64,1}}({[2,3],[2,4],[1,3],[3,4],[1,2],[1,4]})

    [2,3] is in 37.5% of the transactions, thus adding it to result.
    [2,4] is in 37.5% of the transactions, thus adding it to result.
    [1,3] is not in 37.5% of the transactions, thus 
        pruning combs_of_size[3] (= all combos of size 3)
            before: Set{Array{Int64,1}}({[1,2,3],[2,3,4],[1,2,4],[1,3,4]})
            after: Set{Array{Int64,1}}({[2,3,4],[1,2,4]})
        pruning combs_of_size[4] (= all combos of size 4)
            before: Set{Array{Int64,1}}({[1,2,3,4]})
            after: Set{Array{Int64,1}}({})
    [3,4] is in 37.5% of the transactions, thus adding it to result.
    [1,2] is in 37.5% of the transactions, thus adding it to result.
    [1,4] is not in 37.5% of the transactions, thus 
        pruning combs_of_size[3] (= all combos of size 3)
            before: Set{Array{Int64,1}}({[2,3,4],[1,2,4]})
            after: Set{Array{Int64,1}}({[2,3,4]})

    finished iteration for combinations of size 2, current result is:
    {[2],[1],[3],[4],[2,3],[2,4],[3,4],[1,2]}
    ------------------ looking at all combinations of size 3 (combs_of_size[n]) -----------------
    those are Set{Array{Int64,1}}({[2,3,4]})

    [2,3,4] is not in 37.5% of the transactions, thus 

    finished iteration for combinations of size 3, current result is:
    {[2],[1],[3],[4],[2,3],[2,4],[3,4],[1,2]}
    ------------------ looking at all combinations of size 4 (combs_of_size[n]) -----------------
    those are Set{Array{Int64,1}}({})


    finished iteration for combinations of size 4, current result is:
    {[2],[1],[3],[4],[2,3],[2,4],[3,4],[1,2]}
    transactions => {{1,2,3,4},{1,2,4},{1,2},{2,3,4},{2,3},{3,4},{2,4}}
    result => {[2],[1],[3],[4],[2,3],[2,4],[3,4],[1,2]}


